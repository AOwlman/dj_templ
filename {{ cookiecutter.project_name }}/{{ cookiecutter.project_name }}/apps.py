from django.apps import AppConfig


class {{ cookiecutter.project_name.capitalize() }}Config(AppConfig):
    name = '{{ cookiecutter.project_name }}'
