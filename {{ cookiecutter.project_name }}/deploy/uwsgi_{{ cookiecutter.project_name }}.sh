#! /bin/bash
source /home/{{ cookiecutter.user_name }}/{{ cookiecutter.env_name }}/env/bin/activate
exec uwsgi --ini /home/{{ cookiecutter.user_name }}/{{ cookiecutter.env_name }}/{{ cookiecutter.project_name }}/deploy/django_nginx.ini
