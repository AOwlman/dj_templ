from django.apps import AppConfig


class {{ cookiecutter.first_app }}Config(AppConfig):
    name = 'apps.{{ cookiecutter.first_app }}'
    # verbose_name = '<Название приложения для админки>'
