from django.views.generic.base import TemplateView


class {{ cookiecutter.first_app }}IndexView(TemplateView):
    template_name = "{{ cookiecutter.first_app }}-index.html"