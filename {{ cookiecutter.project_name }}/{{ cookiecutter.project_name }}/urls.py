from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('{{ cookiecutter.first_app }}/', include('apps.{{ cookiecutter.first_app }}.urls')),
    path('', views.IndexView.as_view(), name="index"),
]


