#!/bin/bash
source ../env/bin/activate

echo 'Installing pip packages...'
pip install -r requirements/product.txt
pip install -r requirements/debug.txt
echo 'Installing pip packages complete.'
./manage.py migrate
echo 'DB migrate complete.'
echo '----------------------------'
echo 'dj_templ install complete.'
