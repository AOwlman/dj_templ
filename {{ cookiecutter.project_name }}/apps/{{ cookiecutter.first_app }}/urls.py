from django.urls import path

from . import views


urlpatterns = [
    path('',
         views.{{ cookiecutter.first_app }}IndexView.as_view(),
         name="{{ cookiecutter.first_app }}-index"),
]