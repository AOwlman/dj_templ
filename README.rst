Django cookiecutter template
============================

Шаблон Django проекта от owlman (owlman.net)

Использование
-------------

Разверните новое виртуальное окружение:

.. code-block:: shell

    python3 -m venv --system-site-packages env

С помощью `cookiecutter` создайте структуру проекта:

.. code-block:: shell

    pip install cookiecutter
    cookiecutter https://bitbucket.org/AOwlman/dj_templ.git

В ходе создания заполните поля:

- **env_name**: Окружение для нового проекта. То в котором вы разворачиваете проект. ~Обязательное поле~
- **project_name**: название проекта. Так будет назван модуль Python, поэтому выбирайте валидное имя.
- **user_name**: Пользователь в /home которого располагается проект с его окружением. ~Обязательное поле~
- **admin_name** и **admin_email**: контакты администратора. Будут использованы в настройке Django ADMINS.
- **version**: версия с которой начинать проект.
- **production_host**: адрес боевого сервера.
- **site_name**: адрес сайта. Используется в ALLOWED_HOSTS. ~Обязательное поле~
- **local_db_name**, **local_db_user**, **local_db_password**: реквизиты локальной БД для разработки.
- **secret_key**: Секретный ключ Django.
- ... другие настройки из файла cookiecutter.json

При заполнении ~обязательных полей~ проект будет запускаться командой
```
./manage.py runserver 0.0.0.0:8000
```
